﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Google.Apis.Auth.OAuth2;
using System.Threading;
using Google.Apis.Util.Store;
using Google.Apis.Docs.v1;
using Google.Apis.Docs.v1.Data;
using Google.Apis.Services;

namespace ConsoleApp1
{
    class Program
    {
        static string[] Scopes = { DocsService.Scope.Documents };
        static string ApplicationName = "Google Docs API .NET Quickstart";
        static void Main(string[] args)
        {

            UserCredential credential;

            using (var stream =
                new FileStream("credentials.json", FileMode.Open, FileAccess.Read))
            {
                string credPath = "token.json";

                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);
            }

            var service = new DocsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            Console.Write("Enter City: ");
            string input = Console.ReadLine().Replace(" ","%20");
            
            //Console.WriteLine(input[0]);
            string sURL;
            sURL = "https://en.wikipedia.org/w/api.php?action=query&prop=extracts&format=json&titles=" + input + "&redirects=True";
            WebRequest wrGETURL;
            wrGETURL = WebRequest.Create(sURL);
            Stream objStream;
            objStream = wrGETURL.GetResponse().GetResponseStream();
            StreamReader objReader = new StreamReader(objStream);
            //var JSONObj = new JavaScriptSerializer().Deserialize<Dictionary<string, string>>(objReader);

            JObject JSONObj = JObject.Parse(objReader.ReadLine());


            JObject result = JObject.Parse(JSONObj["query"]["pages"].ToString());
            String extract = "";

            if (result.Count > 0) {

                foreach (KeyValuePair<string, JToken> item in result)
                {
                    JToken j = item.Value;
                    //Console.WriteLine(j["extract"]);
                    extract = j["extract"].ToString();
                    break;

                }

            }
            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(extract);

            htmlDoc.DocumentNode.Descendants().Where(n => n.Name == "small").ToList().ForEach(n => n.Remove());

            //Console.WriteLine(htmlDoc.DocumentNode.OuterHtml);





            Location l1 = new Location();
            l1.Index = 1;

            Location l2 = new Location();
            l2.Index = 1;

            InsertTextRequest inserttext = new InsertTextRequest();
            inserttext.Text = htmlDoc.DocumentNode.OuterHtml + "\n";
            inserttext.Location = l1;

            InsertInlineImageRequest insertimage = new InsertInlineImageRequest();
            insertimage.Uri = "http://bit.ly/30v44Mj";
            insertimage.Location = l2;

            Dimension d = new Dimension();
            d.Magnitude = 150;
            d.Unit = "PT";

            Size s = new Size();

            s.Height = d;
            s.Width = d;

            insertimage.ObjectSize = s;

            Request r = new Request();
            r.InsertText = inserttext;

            Request r2 = new Request();
            r2.InsertInlineImage = insertimage;

            List<Request> li = new List<Request>();
            
            li.Add(r2);
            li.Add(r);

            Document doc = new Document();
            doc.Title = input.Replace("%20"," ");
            doc = service.Documents.Create(doc).Execute();
            Console.WriteLine("Title of the doc is " + doc.Title + " and id is " + doc.DocumentId);

            BatchUpdateDocumentRequest body = new BatchUpdateDocumentRequest();
            body.Requests = li;
            BatchUpdateDocumentResponse response = service.Documents.BatchUpdate(body, doc.DocumentId).Execute();
            Console.ReadLine();
        }
    }
}
